<?php

namespace WebskiGosc\LogsViewer\Utils;

use OxidEsales\Eshop\Core\Exception;
use OxidEsales\Eshop\Core\Registry;

class UtilsJson
{
    const JSON_ENCODE_SIMPLE = 0;
    const JSON_ENCODE_UNESCAPED = 1;

    /**
     * @param null $mMsg
     * @param int  $iMode
     */
    public function showJsonAndExit($mMsg = null, $iMode = self::JSON_ENCODE_SIMPLE)
    {
        $oUtils = Registry::getUtils();
        $oUtils->showMessageAndExit($this->encodeJson($mMsg, $iMode));
    }

    /**
     * @param null $mMsg
     * @param int  $iMode
     */
    public function returnJson($mMsg = null, $iMode = self::JSON_ENCODE_SIMPLE)
    {
        return $this->encodeJson($mMsg, $iMode);
    }

    /**
     * @param null $mMsg
     * @param int  $iMode
     *
     * @return false|string
     */
    public function encodeJson($mMsg = null, $iMode = self::JSON_ENCODE_SIMPLE)
    {
        if (is_string($mMsg)) {
            if (!$this->isUtfString($mMsg)) {
                $mMsg = utf8_encode($mMsg);
            }
        } else {
            // Typecast for Objects
            if (is_object($mMsg)) {
                $mMsg = (array) $mMsg;
            }

            $mMsg = $this->_encodeUtf8Array($mMsg);
        }

        $sJson = '';

        switch ($iMode) {
            case 0: //simple json_encode
                $sJson = json_encode($mMsg);
                break;
            case 1: //simple
                $sJson = json_encode($mMsg, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_LINE_TERMINATORS | JSON_UNESCAPED_SLASHES);
                break;
        }

        return $sJson;
    }

    /**
     * @param string $sString
     *
     * @return bool
     */
    public function isUtfString($sString = '')
    {
        if (is_string($sString) && (function_exists('mb_detect_encoding') && false !== mb_detect_encoding($sString, 'UTF-8', true))) {
            return true;
        }

        return false;
    }

    /**
     * @param array $aArray
     *
     * @return array
     */
    protected function _encodeUtf8Array($aArray)
    {
        $aRet = array();

        foreach ($aArray as $sKey => $mValue) {
            if (!$this->isUtfString($mValue)) {
                $sKey = utf8_encode($sKey);
            }

            if (is_string($mValue)) {
                if (!$this->isUtfString($mValue)) {
                    $mValue = utf8_encode($mValue);
                }
            } elseif (is_array($mValue)) {
                $mValue = $this->_encodeUtf8Array($mValue);
            }

            $aRet[$sKey] = $mValue;
        }

        return $aRet;
    }

    public static function jsonValidate($string, $retAsBool = false)
    {
        // decode the JSON data
        $result = json_decode($string);

        // switch and check possible JSON errors
        switch (json_last_error()) {
            case JSON_ERROR_NONE:
                $error = ''; // JSON is valid // No error has occurred
                break;
            case JSON_ERROR_DEPTH:
                $error = 'The maximum stack depth has been exceeded.';
                break;
            case JSON_ERROR_STATE_MISMATCH:
                $error = 'Invalid or malformed JSON.';
                break;
            case JSON_ERROR_CTRL_CHAR:
                $error = 'Control character error, possibly incorrectly encoded.';
                break;
            case JSON_ERROR_SYNTAX:
                $error = 'Syntax error, malformed JSON.';
                break;
            // PHP >= 5.3.3
            case JSON_ERROR_UTF8:
                $error = 'Malformed UTF-8 characters, possibly incorrectly encoded.';
                break;
            // PHP >= 5.5.0
            case JSON_ERROR_RECURSION:
                $error = 'One or more recursive references in the value to be encoded.';
                break;
            // PHP >= 5.5.0
            case JSON_ERROR_INF_OR_NAN:
                $error = 'One or more NAN or INF values in the value to be encoded.';
                break;
            case JSON_ERROR_UNSUPPORTED_TYPE:
                $error = 'A value of a type that cannot be encoded was given.';
                break;
            default:
                $error = 'Unknown JSON error occured.';
                break;
        }

        if ('' !== $error) {
            // throw the Exception or exit // or whatever :)
            if (true === $retAsBool) {
                return false;
            } else {
                throw new Exception\StandardException($error);
            }
        }

        // everything is OK
        return $result;
    }
}
