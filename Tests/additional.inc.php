<?php

use OxidEsales\Eshop\Core\DatabaseProvider;
use OxidEsales\Eshop\Core\Exception\DatabaseConnectionException;
use OxidEsales\Eshop\Core\Exception\DatabaseErrorException;
use OxidEsales\Eshop\Core\Registry;

function removeDbModOxconf()
{
    $sSql = "DELETE FROM oxconfig WHERE OXVARNAME IN (
                \"aModulePaths\",
                \"aModules\",
                \"aDisabledModules\",
                \"aLegacyModules\",
                \"aModuleTemplates\",
                \"aModuleFiles\",
                \"aModuleVersions\",
                \"aModuleEvents\",
                \"aModulePaths\"
            );";
    /** @var DatabaseProvider $oDb */
    try {
        $oDb = DatabaseProvider::getDb(DatabaseProvider::FETCH_MODE_ASSOC);
        $oDb->execute($sSql);
    } catch (DatabaseErrorException $ex) {
        Registry::getLogger()->alert($ex->getMessage(), array($ex));
        echo "\nSomething going wrong, please check oxid log file.\n";
    } catch (DatabaseConnectionException $ex) {
        Registry::getLogger()->alert($ex->getMessage(), array($ex));
        echo "\nSomething going wrong, please check oxid log file.\n";
    }

    $utilsObject = new \OxidEsales\Eshop\Core\UtilsObject;
    $utilsObject->resetInstanceCache();
    $utilsObject::getInstance()->resetInstanceCache('oxconfig');
    $utilsObject::getInstance()->resetInstanceCache();
    \OxidEsales\Eshop\Core\Registry::set(\OxidEsales\Eshop\Core\UtilsObject::class, $utilsObject);
    \OxidEsales\Eshop\Core\Module\ModuleVariablesLocator::resetModuleVariables();

    \OxidEsales\Eshop\Core\Registry::set(\OxidEsales\Eshop\Core\Config::class, null);
    \OxidEsales\Eshop\Core\Registry::getConfig()->setConfig(null);
    \OxidEsales\Eshop\Core\Registry::set(\OxidEsales\Eshop\Core\Config::class, null);

}

//Executing...
removeDbModOxconf();