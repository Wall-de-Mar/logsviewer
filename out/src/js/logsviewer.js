$(function () {

    "use strict";

    let response = {};

    function ajaxCall(handleData, targetCtrl, targetFnc, params) {
        //get current url
        let href = $(location).attr('href');
        //prepare request url
        let requestUrl = href.split("?")[0] + "?cl=" + targetCtrl;

        let url = new URL(href);
        let shpid = url.searchParams.get("shp");

        if (targetFnc) {
            requestUrl = requestUrl + "&fnc=" + targetFnc;
        }
        if (shpid) {
            requestUrl = requestUrl + "&shp=" + shpid;
        }

        $.ajax({
            type: "POST",
            url: requestUrl,
            dataType: params.dataType,
            data: {
                params: params
            }
        }).done(function (result) {
            console.log('done ajax request...');
            response = handleData(result);
        }).fail(function (jqXHR, exception) {
            console.log("fail AJAX request!");

            var msg = "";
            if (jqXHR.status === 0) {
                msg = "Not connect.\n Verify Network.";
            } else if (jqXHR.status === 404) {
                msg = "Requested page not found. [404]";
            } else if (jqXHR.status === 500) {
                msg = "Internal Server Error [500].";
            } else if (exception === "parsererror") {
                msg = "Requested JSON parse failed.";
            } else if (exception === "timeout") {
                msg = "Time out error.";
            } else if (exception === "abort") {
                msg = "Ajax request aborted.";
            } else {
                msg = "Uncaught Error.\n" + jqXHR.responseText;
            }

        }).always(function () {
            console.log('always ajax request...');
        }).then(function (result) {
            console.log('then part...');
            //console.log(result);
        })

    }

    function getLogs(ajaxResult) {
        console.log('Jestem w getLogs...');
        console.log(ajaxResult);

        let logsContainer = $('.logs_container');
        logsContainer.html('');
        logsContainer.append(ajaxResult);
    }

    let oxidLogsBtn = $("#oxidLogsBtn");
    oxidLogsBtn.on("click", function () {
        console.log('Kliknięto przycisk pobierz logi');
        ajaxCall(getLogs, 'wg_logsviewer_admin_main_ajax', 'getLogs', {dataType: 'html', logsSrc: 'oxid'});
        //console.log(response);
    });

    let phpErrorLogsBtn = $("#phpErrorLogsBtn");
    phpErrorLogsBtn.on("click", function () {
        console.log('Kliknięto przycisk pobierz logi');
        ajaxCall(getLogs, 'wg_logsviewer_admin_main_ajax', 'getLogs', {dataType: 'html', logsSrc: 'php_error'});
        //console.log(response);
    });

    let customBtn = $(".customBtn");
    customBtn.on("click", function () {
        console.log('Kliknięto przycisk pobierz logi - custom: '+$(this).val());
        ajaxCall(getLogs, 'wg_logsviewer_admin_main_ajax', 'getLogs', {dataType: 'html', logsSrc: 'custom', fileName: $(this).val()});
        //console.log(response);
    });

});