[{include file="headitem.tpl" title="GENERAL_ADMIN_TITLE"|oxmultilangassign skip_onload="true"}]

<link rel="stylesheet" href='[{$oViewConf->getModuleUrl("logsviewer", "out/src/css/bootstrap.min.css")}]'>

[{if $readonly}]
    [{assign var="readonly" value="readonly disabled"}]
[{else}]
    [{assign var="readonly" value=""}]
[{/if}]

<form name="transfer" id="transfer" action="[{$oViewConf->getSelfLink()}]" method="post">
    [{$oViewConf->getHiddenSid()}]
    <input type="hidden" name="oxid" value="1">
    <input type="hidden" name="cl" value="">
</form>

<div class="container-fluid">
    <div id="wg_logsviewer_container">
        <h2 class="title"><i class="fas fa-file-code"></i> [{oxmultilang ident="WG_LOGSVIEWER_ADMIN_MAIN"}]</h2>

        <div class="row mb-3">
            <div class="col-sm">
                <button type="button" id="oxidLogsBtn" class="btn btn-primary">Get 'oxideshop' logs</button>
                <button type="button" id="phpErrorLogsBtn" class="btn btn-primary">Get 'php error' logs</button>

                [{if $aCustomLogFiles gt 0}]
                    [{foreach from=$aCustomLogFiles item=sCustomLogFile}]
                        <button type="button" value="[{$sCustomLogFile}]" class="btn btn-primary customBtn">Get '[{$sCustomLogFile}]' logs</button>
                    [{/foreach}]
                [{/if}]
            </div>
        </div>

        <div class="row">
            <div class="col-sm"><section class="logs_container"></section></div>
        </div>

    </div>
</div>

<script type="text/javascript" src="[{$oViewConf->getModuleUrl("logsviewer", "out/src/js/jquery-3.4.1.min.js")}]"></script>
<script type="text/javascript" src="[{$oViewConf->getModuleUrl("logsviewer", "out/src/js/bootstrap.bundle.min.js")}]"></script>
<script type="text/javascript" src="[{$oViewConf->getModuleUrl("logsviewer", "out/src/js/bootstrap.min.js")}]"></script>
<script type="text/javascript" src="[{$oViewConf->getModuleUrl("logsviewer", "out/src/fontawesome-free-5.9.0-web/js/all.min.js")}]"></script>
<script type="text/javascript" src="[{$oViewConf->getModuleUrl("logsviewer", "out/src/js/logsviewer.js")}]"></script>
<script type="text/javascript"></script>