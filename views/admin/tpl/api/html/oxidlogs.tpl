<h3><i class="fas fa-angle-right"></i> OXID error logs</h3>
<div class="list-group">
    [{foreach from=$aLogs.data item=aLog}]

    <a href="#" class="list-group-item list-group-item-action">

        [{assign var="aLogObj" value=$aLog.log}]
        <div class="d-flex w-100 justify-content-between">
            <h5 class="mb-1">[{$aLogObj.sLogDescription}]</h5>
            <div>
                <span class="badge badge-light">[{$aLog.datetime}]</span>
                <span class="badge badge-danger">[{$aLogObj.sLogHeader}]</span>
            </div>
        </div>
        <h6>Exception class:</h6>
        <p class="mb-1">[{$aLogObj.sLogExceptionClass}]</p>
        <h6>Stacktrace:</h6>
        <small><code>
                [{foreach from=$aLogObj.aStacktrace item=sStacktraceElement key=key}]
                    #[{$key}]: [{$sStacktraceElement}]<br>
                [{/foreach}]
            </code></small>

    </a>

    [{/foreach}]
</div>