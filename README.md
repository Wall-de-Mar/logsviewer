####1. Installation

#####A. First option: Add to main composer.json file:
```
"autoload": {
    "psr-4": {
        "WebskiGosc\\LogsViewer\\": "./source/modules/wg/logsviewer"
    }
},
```
and make `composer update --no-plugins --no-scripts` and then `composer update`
#####B. Second option: with composer require
```
composer.phar config repositories.wg/logsviewer vcs https://Wall-de-Mar@bitbucket.org/Wall-de-Mar/logsviewer.git
composer.phar require wg/logsviewer:"v1.0.0"
composer.phar update --no-plugins --no-scripts
composer.phar update
```

####2. Required blocks in template
- none

####3. Required template changes
- none

####4. Module requirements:
- none

####5. Conflicts
- none

####6. Known issues
- none

####7. CRON usage
- none

####8. Used/created Visual CMS idents. in module
- none

####9. How module work?
- none

####10. Module ERROR codes:
- none