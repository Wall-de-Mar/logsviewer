<?php
namespace WebskiGosc\LogsViewer\Model;

use WebskiGosc\LogsViewer\Utils\UtilsJson;
use WebskiGosc\LogsViewer\Model\OxidResponse;
use WebskiGosc\LogsViewer\Model\IResponse;

class Response {

    const SORT_BY_TIME_ASC = 'ASC';
    const SORT_BY_TIME_DESC = 'DESC';

    protected $_sLogPath = '';

    protected $_oResponse = null;

    public function __construct($sLogPath = '')
    {
        $this->_sLogPath = $sLogPath;

        //TODO: w tym miejscu decyzja jaki obiekt zaladowac dla oxid, php i innych
        $this->_oResponse = oxNew($this->_recognizeClassByPath($this->_sLogPath));

        if (!file_exists($this->_sLogPath)) {
            $this->_oResponse->addMsgError('File does not exist in path: ' . $this->_sLogPath);
            //TODO:: return error
        } elseif (!is_readable($this->_sLogPath)) {
            $this->_oResponse->addMsgError('File is not readable in path: ' . $this->_sLogPath);
            //TODO:: return error
        } else {
            $sFileData = file_get_contents($this->_sLogPath);
            $this->_oResponse->setFileData($sFileData);
        }
    }

    public function getLogs() {
        return $this->_oResponse->getResponseData();
    }

    public function getTemplate() {
        return $this->_oResponse->getTemplate();
    }

    protected function _recognizeClassByPath($sPath) {

        $aExplodedPath = explode('/', $sPath);
        $sLastElement = array_pop($aExplodedPath);

        if($sLastElement === 'oxideshop.log') {
            return OxidResponse::class;
        } else if($sLastElement === 'error.log') {
            return PhpErrorResponse::class;
        } else {
            return CustomResponse::class;
        }

    }



}