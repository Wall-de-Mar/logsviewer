<?php

namespace WebskiGosc\LogsViewer\Model;

interface IResponse
{
    public function setFileData(string $sFileData): void;

    public function setStatus(bool $status): bool;

    public function addMsgError(string $msg): void;

    public function addMsgInfo(string $msg): void;

    public function addMsgWarning(string $msg): void;

    public function addData(array $data): void;

    public function sortData(string $sSortBy): void;

    public function split2view(string $sLog): array;

    public function getResponseData(): array;

    public function getTemplate(): string;
}