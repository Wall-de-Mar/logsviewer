<?php
namespace WebskiGosc\LogsViewer\Model;

class PhpErrorResponse {

    const SORT_BY_TIME_ASC = 'ASC';
    const SORT_BY_TIME_DESC = 'DESC';

    protected $_FileData = '';

    protected $_aResponseData = array(
        'status' => false,
        'msg' => [
            'error' => [],
            'info' => [],
            'warning' => []
        ],
        'total_count' => 0,
        'data' => [],
        'sort_by' => '',
    );

    protected $_sThisTemplate = 'wg/logsviewer/admin/api/html/phperrorlogs.tpl';

    public function setFileData(string $sFileData) :void
    {
        $this->_FileData = $sFileData;

        //TODO: tylko tymczasowo
        $this->addData([$sFileData]);
    }

    public function getResponseData(): array
    {
        return $this->_aResponseData;
    }

    public function getTemplate(): string {
        return $this->_sThisTemplate;
    }

    public function addData(array $data): void
    {
        array_push($this->_aResponseData['data'], $data);
        $this->_aResponseData['total_count'] = count($this->_aResponseData['data']);
    }

}