<?php

namespace WebskiGosc\LogsViewer\Model;

use WebskiGosc\LogsViewer\Utils\UtilsJson;

class OxidResponse implements IResponse
{
    const SORT_BY_TIME_ASC = 'ASC';
    const SORT_BY_TIME_DESC = 'DESC';

    protected $_FileData = '';

    protected $_aResponseData = array(
        'status' => false,
        'msg' => [
            'error' => [],
            'info' => [],
            'warning' => []
        ],
        'total_count' => 0,
        'data' => [],
        'sort_by' => '',
    );

    protected $_sThisTemplate = 'wg/logsviewer/admin/api/html/oxidlogs.tpl';

    public function setFileData(string $sFileData) :void
    {
        $this->_FileData = $sFileData;

        $aFileDataArray = explode(PHP_EOL, $sFileData);
        array_pop($aFileDataArray); //remove last empty element

        $sRegexPattern = '/\[(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2}):(\d{2})\]/';
        foreach ($aFileDataArray as $log) {
            $aKeys = [];
            preg_match_all($sRegexPattern, $log, $aKeys);
            $sDateTimeLog = $aKeys[0][0]; //get only full matched string
            $sDateTimeCleared = str_replace('[', '', $sDateTimeLog);
            $sDateTimeCleared = str_replace(']', '', $sDateTimeCleared);

            $sLogWithoutDateTime = trim(str_replace($sDateTimeLog, '', $log));

            $aResData = array();
            $aResData['datetime'] = $sDateTimeCleared;
            $aResData['log'] = $this->split2view($sLogWithoutDateTime);
            $this->addData($aResData);
        }
        //$this->sortData();
        $this->setStatus(true);
    }

    public function getResponseData(): array
    {
        return $this->_aResponseData;
    }

    public function setStatus(bool $status): bool
    {
        return $this->_aResponseData['status'] = $status;
    }

    public function addMsgError(string $msg): void
    {
        $this->_aResponseData['msg']['error'][] = $msg;
    }

    public function addMsgInfo(string $msg): void
    {
        $this->_aResponseData['msg']['info'][] = $msg;
    }

    public function addMsgWarning(string $msg): void
    {
        $this->_aResponseData['msg']['warning'][] = $msg;
    }

    public function addData(array $data): void
    {
        array_push($this->_aResponseData['data'], $data);
        $this->_aResponseData['total_count'] = count($this->_aResponseData['data']);
    }

    public function sortData(string $sSortBy = self::SORT_BY_TIME_DESC): void
    {
        switch ($sSortBy) {
            case self::SORT_BY_TIME_DESC:
                $this->_aResponseData['sort_by'] = self::SORT_BY_TIME_DESC;
                usort($this->_aResponseData['data'], function ($item1, $item2) {
                    return strtotime($item2['datetime']) <=> strtotime($item1['datetime']);
                });
                break;
            case self::SORT_BY_TIME_ASC;
                $this->_aResponseData['sort_by'] = self::SORT_BY_TIME_ASC;
                usort($this->_aResponseData['data'], function ($item1, $item2) {
                    return strtotime($item1['datetime']) <=> strtotime($item2['datetime']);
                });
                break;
        }
    }

    public function getTemplate(): string {
        return $this->_sThisTemplate;
    }

    public function split2view(string $sLog): array
    {
        $aLog = [];

        /**
         * LOG HEADER
         * ^ asserts position at start of a line
         * OXID Logger matches the characters OXID Logger literally (case sensitive)
         * \. matches the character . literally (case sensitive)
         * \w* matches any word character (equal to [a-zA-Z0-9_])
         *  * Quantifier — Matches between zero and unlimited times, as many times as possible, giving back as needed (greedy)
         * \: matches the character : literally (case sensitive)
         * \s matches any whitespace character (equal to [\r\n\t\f\v ])
         */
        $sLogHeaderPattern = '/OXID Logger\.\w*\:\s/';
        $matches = [];
        preg_match($sLogHeaderPattern, $sLog, $matches);
        $sLogHeader = trim($matches[0]);

        //remove header from log
        $sLog = trim(str_replace($sLogHeader, '', $sLog));

        $sLogHeader = trim(str_replace(':', '', $sLogHeader));


        /**
         * LOG ELEMENT
         * \[ matches the character [ literally (case sensitive)
         * \" matches the character " literally (case sensitive)
         * \[ matches the character [ literally (case sensitive)
         * .* matches any character (except for line terminators)
         * * Quantifier — Matches between zero and unlimited times, as many times as possible, giving back as needed
         */
        $sLogElementPattern = '/\[\"\[.*\[\]/';
        $matches = [];
        preg_match($sLogElementPattern, $sLog, $matches);
        $sLogElement = trim($matches[0]);

        /**
         * LOG DESCRIPTION
         */
        $sLogDescription = trim(str_replace($sLogElement, '', $sLog));

        /**
         * LOG EXCEPTION STACKTRACE
         */
        $sStacktracePattern = '/(\[stacktrace\](?![\n])).*(\"\]\s\[\])/';
        $matches = [];
        preg_match($sStacktracePattern, $sLogElement, $matches);
        $sStacktrace = trim($matches[0]);
        // remove '[stacktrace]\n'
        $sClearedStacktrace = trim(str_replace('[stacktrace]\n', '', $sStacktrace));
        // split by '#0 ', '#1 ', etc. and remove first empty element
        $sStacktraceSplitPattern = '/\#\d+\s/';
        $aStacktrace = preg_split($sStacktraceSplitPattern, $sStacktrace);
        array_shift($aStacktrace);
        // remove '\n' from each line
        $aStacktrace = array_map(
            function ($str) {
                return str_replace('\n', '', $str);
            },
            $aStacktrace
        );

        /**
         * LOG EXCEPTION CLASS
         */
        $sLogExceptionClass = trim(str_replace($sStacktrace, '', $sLogElement));
        //remove '\n' from the end of string
        $sLogExceptionClass = trim(str_replace('\n', '', $sLogExceptionClass));
        // remove '["[object] '
        $sLogExceptionClass = trim(preg_replace('/\[\"\[.*\]\s/', '', $sLogExceptionClass));

        /*var_dump($sLogHeaderPattern);
        var_dump("sLogHeader: ".$sLogHeader);
        var_dump("matches: ".$matches);
        var_dump("sLogElement: ".$sLogElement);
        var_dump("sLogDescription: ".$sLogDescription);
        var_dump("sStacktrace: ".$sStacktrace);
        var_dump($aStacktrace);
        var_dump("sLogExceptionClass: ".$sLogExceptionClass);
        var_dump("############################");*/

        $aLog['sLogHeader'] = $sLogHeader;
        $aLog['sLogDescription'] = $sLogDescription;
        $aLog['sLogExceptionClass'] = $sLogExceptionClass;
        $aLog['aStacktrace'] = $aStacktrace;

        return $aLog;
    }

}
