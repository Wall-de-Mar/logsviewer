<?php
namespace WebskiGosc\LogsViewer\Controller\Admin;

use OxidEsales\Eshop\Application\Controller\Admin\AdminController;
use OxidEsales\Eshop\Core\Registry;
use WebskiGosc\LogsViewer\Model\Response;

class MainController extends AdminController {

    protected $_sThisTemplate = 'wg/logsviewer/admin/main.tpl';

    public function render(): string
    {
        parent::render();

        //$blDebugMigrationToFile = (bool) $this->getConfig()->getConfigParam('blDebugMigrationToFile');
        $sCustomLogFiles = $this->getConfig()->getConfigParam('sCustomLogFiles');
        $aCustomLogFiles = explode(';',$sCustomLogFiles);

        $aCustomLogFilesWithoutExtension = array_map([$this,'removeExtension'], $aCustomLogFiles);
        //$this->setViewData('aCustomLogFilesWithoutExtension', $aCustomLogFilesWithoutExtension);
        $this->_aViewData['aCustomLogFilesWithoutExtension'] = $aCustomLogFilesWithoutExtension;
        $this->_aViewData['aCustomLogFiles'] = $aCustomLogFiles;

        return $this->_sThisTemplate;
    }

    protected function removeExtension($sFileName) {
         return $filename = pathinfo($sFileName, PATHINFO_FILENAME);
    }

}