<?php

namespace WebskiGosc\LogsViewer\Controller\Admin;

use OxidEsales\Eshop\Core\Controller\BaseController;
use OxidEsales\Eshop\Core\Registry;
use WebskiGosc\LogsViewer\Model\Response;
use WebskiGosc\LogsViewer\Utils\UtilsJson;

// Load bootstrap
/*$bootstrapFilePath = __DIR__ . '/../../../../../bootstrap.php';
var_dump($bootstrapFilePath);
require realpath($bootstrapFilePath);*/

//TODO: Czy BaseController jest potrzebny? Zrobić zapezpieczenie przez wywolaniem ajax controlelr'a
class MainAjaxController extends BaseController
{

    /** @var Response null */
    protected $_oResponse = null;

    /*public function init()
    {
        parent::init();

        $oUser = Registry::getSession()->getUser();

        //Check if requested user is admin
        if(!$oUser || !$oUser->isAdmin()) {
            Registry::getUtils()->showMessageAndExit('Access denied');
        }
    }*/

    public function getLogs()
    {
        $oConfig = Registry::getConfig();
        $oRequest = Registry::getRequest();
        $aRequestParams = $oRequest->getRequestEscapedParameter('params');

        $sLogFileName = '';
        if($aRequestParams['logsSrc'] === 'oxid') {
            $sLogFileName = 'oxideshop.log';
        } elseif ($aRequestParams['logsSrc'] === 'php_error') {
            $sLogFileName = 'error.log';
        } elseif($aRequestParams['logsSrc'] === 'custom') {
            $sLogFileName = $aRequestParams['fileName'];
        }

        $sLogPath = $oConfig->getConfigParam('sShopDir').'log/'.$sLogFileName;

        $oResponse = oxNew(Response::class, $sLogPath);
        $this->_oResponse = $oResponse;
    }

    public function render(): string
    {
        parent::render();

        $oRequest = Registry::getRequest();
        $aRequestParams = $oRequest->getRequestEscapedParameter('params');

        if (!empty($aRequestParams)) {
            if ($aRequestParams['dataType'] === 'html') {
                $oSmarty = Registry::getUtilsView()->getSmarty();
                $oSmarty->assign('aLogs', $this->_oResponse->getLogs());
                $smartyResult = $oSmarty->fetch($this->_oResponse->getTemplate());
                Registry::getUtils()->showMessageAndExit($smartyResult);
            } elseif ($aRequestParams['dataType'] === 'json') {
                $this->showJsonAndExit();
            }
        }

    }

    public function showJsonAndExit() {
        $oUtilsJson = oxNew(UtilsJson::class);
        $oUtilsJson->showJsonAndExit($this->_oResponse->getLogs());
    }

    public function returnJson() {
        $oUtilsJson = oxNew(UtilsJson::class);
        return $oUtilsJson->returnJson($this->_oResponse->getLogs());
    }

}

/*$x = new MainAjaxController();
echo $x->getLogs();*/