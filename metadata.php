<?php

/**
 * Metadata version.
 */
$sMetadataVersion = '2.1';

$aModule = array(
    'id' => 'logsviewer',
    'title' => 'webskigosc.com - Logs Viewer',
    'description' => array(
        'de' => 'Modul zum Anzeigen von Fehlerprotokollen.',
        'en' => 'Module for view error logs.',
    ),
    'thumbnail' => 'module-kiratik.png',
    'version' => '1.0.2',
    'author' => 'Waldemar Stańko',
    'url' => 'https://www.webskigosc.com',
    'email' => 'napiszdo@webskigosc.com',
    'extend' => array(),
    'controllers' => array(
        'wg_logsviewer_admin_main' => WebskiGosc\LogsViewer\Controller\Admin\MainController::class,
        'wg_logsviewer_admin_main_ajax' => WebskiGosc\LogsViewer\Controller\Admin\MainAjaxController::class,
    ),
    'templates' => array(
        'wg/logsviewer/admin/main.tpl' => 'wg/logsviewer/views/admin/tpl/main.tpl',
        'wg/logsviewer/admin/api/html/oxidlogs.tpl' => 'wg/logsviewer/views/admin/tpl/api/html/oxidlogs.tpl',
        'wg/logsviewer/admin/api/html/phperrorlogs.tpl' => 'wg/logsviewer/views/admin/tpl/api/html/phperrorlogs.tpl',
        'wg/logsviewer/admin/api/html/customlogs.tpl' => 'wg/logsviewer/views/admin/tpl/api/html/customlogs.tpl',
    ),
    'blocks' => array(),
    'events' => array(),
    'settings' => array(
        array(
            'group' => 'general',
            'name' => 'sOxidLogFile',
            'type' => 'str',
            'value' => 'oxideshop.log',
        ),
        array(
            'group' => 'general',
            'name' => 'sPhpErrorLogFile',
            'type' => 'str',
            'value' => 'error.log',
        ),
        array(
            'group' => 'general',
            'name' => 'sCustomLogFiles',
            'type' => 'str',
            'value' => '',
        ),
    ),
);
